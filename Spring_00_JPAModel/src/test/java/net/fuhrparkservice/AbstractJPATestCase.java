package net.fuhrparkservice;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;

public abstract class AbstractJPATestCase {

	private static EntityManagerFactory managerFactory;
	protected EntityManager manager;

	@BeforeClass
	public static void setUpEMFactory() {
		managerFactory = Persistence.createEntityManagerFactory("projectUnit");
	}

	@Before
	public void setUpEM() throws Exception {
		manager = managerFactory.createEntityManager();
		setUPTransaction();
		setUpTestData();
	}
	
	private void setUPTransaction(){
		manager.getTransaction().begin();
	}
	
	public void setUpTestData() throws Exception{
		this.setUp();
	}
	
	protected abstract void setUp() throws Exception;

	@After
	public void transactionRollback(){
		if(manager.getTransaction().isActive())
			manager.getTransaction().rollback();
	}

}
