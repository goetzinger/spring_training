package net.fuhrparkservice.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "tbl_fahrzeugItem")
public class FahrzeugItem extends AbstractBusinessObject{

	@ManyToOne(cascade = CascadeType.PERSIST)
	private Fahrzeug type;
	@ManyToOne(cascade = CascadeType.PERSIST)
	private Filiale standort;
	@ManyToMany(cascade={CascadeType.PERSIST,CascadeType.MERGE})
	@JoinTable(name = "tbl_fahrzeugItem_2_standort", joinColumns = { @JoinColumn(name = "fahrzeugItem_id") }, inverseJoinColumns = { @JoinColumn(name = "filiale_id") })
	private List<Filiale> standortHistory = new ArrayList<Filiale>();

	public List<Filiale> getStandortHistory() {
		return standortHistory;
	}

	public void setStandortHistory(List<Filiale> standortHistory) {
		this.standortHistory = standortHistory;
	}

	public FahrzeugItem() {
	}

	public FahrzeugItem(String id, Filiale standort, Fahrzeug type) {
		super(id);
		setStandort(standort);
		this.type = type;
	}

	public void setType(Fahrzeug type) {
		this.type = type;
	}

	public Fahrzeug getType() {
		return type;
	}

	public void setStandort(Filiale standort) {
		if (this.standort != null)
		{
			this.standortHistory.add(this.standort);
			this.standort.getFahrzeuge().remove(this);
		}
		this.standort = standort;
		
	}

	public Filiale getStandort() {
		return standort;
	}
}
