package net.fuhrparkservice.model;

import java.io.Serializable;
import java.util.UUID;

import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public class AbstractBusinessObject implements Serializable{

	@Id
	private String id;

	public AbstractBusinessObject() {
		this(UUID.randomUUID().toString());
	}

	public AbstractBusinessObject(String id) {
		this.id = id;
	}

	public String getId() {
		return id;
	}

}
