/**
 * 
 */
package net.fuhrparkservice.model;

import javax.persistence.Embeddable;

/**
 * @author goetzingert
 * 
 */
@Embeddable
public class Model {

	private String fabrikat;
	private String modell;

	public Model() {
	}

	public Model(String fabrikat, String modell) {
		super();
		this.fabrikat = fabrikat;
		this.modell = modell;
	}

	public String getFabrikat() {
		return fabrikat;
	}

	public void setFabrikat(String fabrikat) {
		this.fabrikat = fabrikat;
	}

	public String getModell() {
		return modell;
	}

	public void setModell(String modell) {
		this.modell = modell;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj != null && obj instanceof Model) {
			Model other = (Model) obj;
			if (this.fabrikat.equals(other.fabrikat)
					&& this.modell.equals(other.modell))
				return true;
		}
		return false;
	}

	@Override
	public int hashCode() {
		int hashCode = fabrikat.hashCode();
		hashCode *= modell.hashCode();
		return hashCode;
	}

	@Override
	public String toString() {
		return this.fabrikat + " : " + this.modell;
	}
}
