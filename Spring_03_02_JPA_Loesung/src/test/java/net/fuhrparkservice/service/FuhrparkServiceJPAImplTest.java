package net.fuhrparkservice.service;

import java.util.List;

import javax.annotation.Resource;

import net.fuhrparkservice.model.FahrzeugType;
import net.fuhrparkservice.model.Model;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "/applicationContext.xml" })
@Transactional
public class FuhrparkServiceJPAImplTest {

	@Resource(name = "fahrzeugCRUD")
	private IFahrzeugCRUD fahrzeugService;

	@Before
	public void insertTestData() {
		fahrzeugService.insert(new FahrzeugType("1", new Model("VW", "Golf"),
				105, 190));
		fahrzeugService.insert(new FahrzeugType("2", new Model("BMW",
				"Korrekte 3er aldem"), 135, 210));
		fahrzeugService.insert(new FahrzeugType("3", new Model("BMW", "6er"),
				225, 230));
		fahrzeugService.insert(new FahrzeugType("4", new Model("Mercedes",
				"SLC"), 210, 230));
		fahrzeugService.insert(new FahrzeugType("5", new Model("Opel",
				"Ohne Power ewig letzter"), 90, 180));
	}

	@Test
	public void jpaTest() throws Exception {
		List<FahrzeugType> vws = fahrzeugService.getFahrzeugeByFabrikat("VW");
		Assert.assertFalse(vws.isEmpty());
	}

}
