package net.fuhrparkservice.service;

import java.util.List;

import javax.annotation.Resource;

import net.fuhrparkservice.model.FahrzeugType;
import net.fuhrparkservice.model.Filiale;

import org.springframework.stereotype.Component;

@Component("verleihService")
public class VerleihServiceAnnotationImpl implements IVerleihService {

	@Resource(name = "fahrzeugService")
	private IFahrzeugService fahrzeugService;

	@Override
	public List<FahrzeugType> getAngeboteneFahrzeuge() {
		return fahrzeugService.getAllFahrzeuge();
	}

	@Override
	public List<Filiale> getFilialen() {
		return null;
	}

	@Override
	public void setSelectedFahrzeug(String id) {

	}

	@Override
	public void setSelectedFiliale(String id) {
	}

	@Override
	public boolean reserviere() throws ReservationException {
		return false;
	}

	public IFahrzeugService getFahrzeugService() {
		return fahrzeugService;
	}

}
