package net.fuhrparkservice.service;

import junit.framework.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = DependencyInjectionTestAnnotation.class)
@Configuration
@ComponentScan("net.fuhrparkservice.service")
public class DependencyInjectionTestAnnotation {

	@Autowired
	private IVerleihService verleihService;

	@Test
	public void dependencyTest() throws Exception {
		Assert.assertNotNull("Verleihservice is null", verleihService);
		Assert.assertNotNull("Fahrzeugservice is null",
				((VerleihServiceAnnotationImpl) verleihService)
						.getFahrzeugService());
	}


}
