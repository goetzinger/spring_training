package net.fuhrparkservice.service;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "/applicationContext.xml" })
public class DependencyInjectionTest{

	@Autowired
	private ApplicationContext context;

	@Autowired
	private IVerleihService verleihService;

	@Test
	public void dependencyTest() throws Exception {
		Assert.assertNotNull("Verleihservice is null", verleihService);
		Assert.assertNotNull("Fahrzeugservice is null",
				((VerleihServiceAnnotationImpl) verleihService)
						.getFahrzeugService());
	}

}
