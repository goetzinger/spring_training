package net.fuhrparkservice.service;

import org.junit.Assert;

import org.junit.Before;
import org.junit.Test;


public class DependencyInjectionTest
{
  private IVerleihService verleihService;
  
  @Test
  public void dependencyTest() throws Exception
  {
    Assert.assertNotNull("Verleihservice is null", verleihService);
    Assert.assertNotNull("Fahrzeugservice is null", verleihService.getAngeboteneFahrzeuge());
  }

  @Before
  public void setUp() throws Exception
  {
    // Hier soll der Spring Container initialisiert werden
  }
}
