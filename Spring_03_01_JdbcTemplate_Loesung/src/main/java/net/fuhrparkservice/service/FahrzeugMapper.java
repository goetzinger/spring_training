package net.fuhrparkservice.service;

/**
 * 
 */

import java.sql.ResultSet;
import java.sql.SQLException;

import net.fuhrparkservice.model.FahrzeugType;
import net.fuhrparkservice.model.Model;

import org.springframework.jdbc.core.simple.ParameterizedRowMapper;

/**
 * @author goetzingert
 */
public class FahrzeugMapper implements ParameterizedRowMapper<FahrzeugType> {

	private static final String MAXKMH_COLUMN_NAME = "maxKmH";
	private static final String PS_COLUMN_NAME = "ps";
	private static final String MODELL_COLUMN_NAME = "modell";
	private static final String FABRIKAT_COLUMN_NAME = "fabrikat";
	private static final String ID_COLUMN_NAME = "ID";

	public FahrzeugType mapRow(ResultSet rs, int rowNum) throws SQLException {

		String id = rs.getString(ID_COLUMN_NAME);
		String fabrikat = rs.getString(FABRIKAT_COLUMN_NAME);
		String modell = rs.getString(MODELL_COLUMN_NAME);
		long ps = rs.getLong(PS_COLUMN_NAME);
		long maxKmH = rs.getLong(MAXKMH_COLUMN_NAME);

		FahrzeugType toReturn = new FahrzeugType(id, new Model(fabrikat, modell), ps,
				maxKmH);

		return toReturn;
	}
}