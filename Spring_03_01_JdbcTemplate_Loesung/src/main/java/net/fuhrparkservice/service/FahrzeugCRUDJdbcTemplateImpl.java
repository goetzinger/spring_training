package net.fuhrparkservice.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.fuhrparkservice.model.FahrzeugType;

import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

/**
 * @author goetzingert
 */
public class FahrzeugCRUDJdbcTemplateImpl implements IFahrzeugCRUD {
	private NamedParameterJdbcTemplate jdbcTemplate;
	private FahrzeugMapper fahrzeugMapper = new FahrzeugMapper();

	public FahrzeugCRUDJdbcTemplateImpl(NamedParameterJdbcTemplate jdbcTemplate) {
		super();
		this.jdbcTemplate = jdbcTemplate;
	}

	@Override
	public List<FahrzeugType> getFahrzeugeByFabrikat(String fabrikat) {
		Map<String, Object> parameter = new HashMap<String, Object>();
		parameter.put("fabrikat", fabrikat);
		List<FahrzeugType> fahrzeuge = jdbcTemplate.query(
				"SELECT DISTINCT * FROM TBL_FahrzeugType f WHERE f.fabrikat = :fabrikat",
				parameter, fahrzeugMapper);
		return fahrzeuge;
	}

	@Override
	public List<FahrzeugType> getAllFahrzeuge() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public FahrzeugType getFahrzeugById(String id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void insert(FahrzeugType fahrzeug2Insert) {
		// TODO Auto-generated method stub

	}

	@Override
	public void update(FahrzeugType fahrzeug2Update) {
		// TODO Auto-generated method stub

	}

	@Override
	public void remove(String id) {
		// TODO Auto-generated method stub

	}

}
