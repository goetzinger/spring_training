package net.fuhrparkservice.service;

import java.util.List;

import net.fuhrparkservice.model.FahrzeugType;
import net.fuhrparkservice.model.Filiale;

public class VerleihServiceAnnotationImpl implements IVerleihService {

	private IFahrzeugService fahrzeugService;

	@Override
	public List<FahrzeugType> getAngeboteneFahrzeuge() {
		return fahrzeugService.getAllFahrzeuge();
	}

	@Override
	public List<Filiale> getFilialen() {
		return null;
	}

	@Override
	public void setSelectedFahrzeug(String id) {

	}

	@Override
	public void setSelectedFiliale(String id) {
	}

	@Override
	public boolean reserviere() throws ReservationException {
		return false;
	}

	public IFahrzeugService getFahrzeugService() {
		return fahrzeugService;
	}

}
