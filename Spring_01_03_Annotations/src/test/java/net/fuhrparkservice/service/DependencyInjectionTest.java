package net.fuhrparkservice.service;

import junit.framework.Assert;
import net.fuhrparkservice.service.VerleihServiceAnnotationImpl;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(loader=AnnotationConfigContextLoader.class, classes=DependencyInjectionTest.class)
@Configuration
@ComponentScan("net.fuhrparkservice")
public class DependencyInjectionTest
{
  private IVerleihService verleihService;
  
  @Test
  public void dependencyTest() throws Exception
  {
    Assert.assertNotNull("Verleihservice is null", verleihService);
    Assert.assertNotNull("Fahrzeugservice is null", ((VerleihServiceAnnotationImpl)verleihService).getFahrzeugService());
  }


}
