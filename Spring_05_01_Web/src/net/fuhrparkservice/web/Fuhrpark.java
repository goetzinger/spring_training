package net.fuhrparkservice.web;

import java.util.List;

import javax.annotation.Resource;

import net.fuhrparkservice.model.FahrzeugType;
import net.fuhrparkservice.service.IFahrzeugCRUD;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller()
@RequestMapping("/fahrzeuge")
public class Fuhrpark {
	
	@Resource
	private IFahrzeugCRUD fahrzeugCRUD;

	@RequestMapping(value="/getAll")
	@ResponseBody
	public List<FahrzeugType> getAllFahrzeuge()
	{
		return fahrzeugCRUD.getAllFahrzeuge();
	}
	
}
