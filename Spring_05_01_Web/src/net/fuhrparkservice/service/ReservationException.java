package net.fuhrparkservice.service;


public class ReservationException extends Exception {

	public ReservationException(String message) {
		super(message);
	}
}
