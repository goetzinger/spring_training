package net.fuhrparkservice.service;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Repository;

import net.fuhrparkservice.model.FahrzeugType;

@Repository
public class FahrzeugCRUDJPAImpl implements IFahrzeugCRUD {

	@PersistenceContext
	private EntityManager manager;

	@Override
	public List<FahrzeugType> getAllFahrzeuge() {
		return manager.createQuery("SELECT f FROM FahrzeugType f", FahrzeugType.class)
				.getResultList();
	}

	@Override
	public FahrzeugType getFahrzeugById(String id) {
		return manager.find(FahrzeugType.class, id);
	}

	@Override
	public List<FahrzeugType> getFahrzeugeByFabrikat(String fabrikat) {
		return manager
				.createQuery(
						"SELECT f FROM FahrzeugType f WHERE f.model.fabrikat = :fabrikat",
						FahrzeugType.class).setParameter("fabrikat", fabrikat)
				.getResultList();
	}

	@Override
	public void insert(FahrzeugType fahrzeug2Insert) {
		manager.persist(fahrzeug2Insert);
	}

	@Override
	public void update(FahrzeugType fahrzeug2Update) {
		manager.merge(fahrzeug2Update);
	}

	@Override
	public void remove(String id) {
		FahrzeugType fahrzeug2Remove = manager.find(FahrzeugType.class, id);
		manager.remove(fahrzeug2Remove);
	}

}
