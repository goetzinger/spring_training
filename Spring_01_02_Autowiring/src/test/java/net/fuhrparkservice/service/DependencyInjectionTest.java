package net.fuhrparkservice.service;

import junit.framework.Assert;
import net.fuhrparkservice.service.IVerleihService;

import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;


public class DependencyInjectionTest
{
  private IVerleihService verleihService;
  
  @Test
  public void dependencyTest() throws Exception
  {
    Assert.assertNotNull("Verleihservice is null", verleihService);
    Assert.assertNotNull("Fahrzeugservice is null", verleihService.getAngeboteneFahrzeuge());
  }

  @Before
  public void setUp() throws Exception
  {
   ApplicationContext applicationContext = new ClassPathXmlApplicationContext("applicationContext.xml");
   verleihService = applicationContext.getBean(IVerleihService.class);
  }
}
