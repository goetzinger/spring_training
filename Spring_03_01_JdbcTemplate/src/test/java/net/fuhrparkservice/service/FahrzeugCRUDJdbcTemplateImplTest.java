package net.fuhrparkservice.service;

import java.util.List;

import javax.annotation.Resource;

import net.fuhrparkservice.model.FahrzeugType;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "/applicationContext.xml" })
public class FahrzeugCRUDJdbcTemplateImplTest {

	@Resource(name = "fahrzeugDao")
	private IFahrzeugCRUD fahrzeugService;

	@Test
	public void aopTest() throws Exception {
		List<FahrzeugType> vws = fahrzeugService.getFahrzeugeByFabrikat("VW");
		Assert.assertFalse(vws.isEmpty());
	}

}
