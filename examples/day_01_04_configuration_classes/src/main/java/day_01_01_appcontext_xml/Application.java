package day_01_01_appcontext_xml;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import externals.FarmerService;
import externals.WifeOfFarmerService;

public class Application {
	
	public static void main(String[] args) {
		ApplicationContext applicationContext = 
				new AnnotationConfigApplicationContext("day_01_01_appcontext_xml");
		FarmService bean = applicationContext.getBean(FarmService.class);
		bean.feed();
		
		FarmerService farmerService = applicationContext.getBean(FarmerService.class);
		WifeOfFarmerService wife = applicationContext.getBean(WifeOfFarmerService.class);
		
		System.out.println(farmerService.getService() == wife.getCowService());
		
	}

}
