package day_01_01_appcontext_xml;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

import day_01_01_appcontext_xml.service.PigService;
import externals.CowService;
import externals.FarmerService;
import externals.WifeOfFarmerService;

@Configuration
public final class ExternalServiceConfiguration {
	
	@Bean
	@Autowired
	public FarmerService farmer( CowService cowService){
		System.out.println("Called farm");
		return new FarmerService(cowService);
	}
	
	@Bean
	public WifeOfFarmerService wife(@Autowired CowService cowService){
		System.out.println("Called wife");
		return new WifeOfFarmerService(cowService);
	}
	
	@Bean
	@Scope("prototype")
	public CowService cowService(){
		System.out.println("Called ");
		return new CowService();
	}
}
