package externals;

public class WifeOfFarmerService {
	
	private CowService cowService;

	public WifeOfFarmerService(CowService cowService) {
		super();
		this.cowService = cowService;
	}
	
	public CowService getCowService() {
		return cowService;
	}

}
