package externals;

public class FarmerService {
	
	private CowService service;

	public FarmerService(CowService service) {
		super();
		this.service = service;
	}
	
	
	public CowService getService() {
		return service;
	}

}
