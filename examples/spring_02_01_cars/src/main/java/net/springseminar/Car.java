package net.springseminar;

public class Car {

	
	private String model;
	private String brand;
	private int hp;
	private int maxKmH;
	
	public Car() {
	}
	
	public Car(String model, String brand, int hp, int maxKmH) {
		super();
		this.model = model;
		this.brand = brand;
		this.hp = hp;
		this.maxKmH = maxKmH;
	}
	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}
	public String getBrand() {
		return brand;
	}
	public void setBrand(String brand) {
		this.brand = brand;
	}
	public int getHp() {
		return hp;
	}
	public void setHp(int hp) {
		this.hp = hp;
	}
	public int getMaxKmH() {
		return maxKmH;
	}
	public void setMaxKmH(int maxKmH) {
		this.maxKmH = maxKmH;
	}

	@Override
	public String toString() {
		return "Car [model=" + model + ", brand=" + brand + ", hp=" + hp
				+ ", maxKmH=" + maxKmH + "]";
	}
	
	
}
