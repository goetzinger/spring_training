package net.springseminar;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Spring0201CarsApplication {

	public static void main(String[] args) {
		SpringApplication.run(Spring0201CarsApplication.class, args);
	}
}
