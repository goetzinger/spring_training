package day_01_01_appcontext_xml;

public class Pig {
	
	
	
	
	public Pig(String name, int weight) {
		super();
		this.name = name;
		this.weight = weight;
	}

	private String name;
	
	private int weight;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getWeight() {
		return weight;
	}

	public void setWeight(int weight) {
		this.weight = weight;
	}
	
	

}
