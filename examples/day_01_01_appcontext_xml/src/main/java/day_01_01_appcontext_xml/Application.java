package day_01_01_appcontext_xml;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Application {
	
	public static void main(String[] args) {
		ClassPathXmlApplicationContext applicationContext = 
				new ClassPathXmlApplicationContext("applicationContext.xml");
		FarmService bean = applicationContext.getBean(FarmService.class);
		bean.feed();
	}

}
