package day_01_01_appcontext_xml;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class PigService {
	
	
	public PigService() {
		System.out.println(this.getClass().getName() + " is created");
	}
	
	public List<Pig> pigs = new ArrayList<Pig>();
	
	public List<Pig> getInhabitants(){
		return Collections.unmodifiableList(pigs);
	}
	
	public void insert(Pig newPig){
		pigs.add(newPig);
	}

}