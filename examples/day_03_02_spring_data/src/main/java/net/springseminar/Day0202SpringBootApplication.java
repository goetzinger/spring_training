package net.springseminar;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaRepositories(basePackageClasses=PigRepository.class)
public class Day0202SpringBootApplication {

	public static void main(String[] args) {
		SpringApplication.run(Day0202SpringBootApplication.class, args);
	}
}
