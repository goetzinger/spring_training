package net.springseminar;

import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("/pig")
@RestController
public class PigController {

	@Autowired
	private PigRepository service;
	
	@PostConstruct
	public void afterInit(){
		System.out.println(service);
	}

	// @RequestMapping(method=RequestMethod.GET)
	@GetMapping
	// GET http://localhost:8080/pig
	public List<Pig> getall() {
		return service.findAll();
	}

	@PostMapping()
	public void insert(@RequestBody Pig newOne) {
		service.save(newOne);
	}

	@GetMapping("/filter")
	// localhost:8080/pig/filter?weight=100
	public List<Pig> findByName(
			@RequestParam(name = "weight") int weightToSearchFor) {
		return service.findByWeightGreaterThan(weightToSearchFor);
	}

	@GetMapping("/filter/weight/{weight}")
	// localhost:8080/pig/filter?weight=100
	public List<Pig> findByNameWithPathVariable(
			@PathVariable(name = "weight") int weightToSearchFor) {
		return service.findAll().stream()
				.filter(pig -> pig.getWeight() > weightToSearchFor)
				.collect(Collectors.toList());
	}

}
