package net.springseminar;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

public interface PigRepository extends JpaRepository<Pig, String> {
	
	List<Pig> findByWeightGreaterThan(int lowerbound);

}
