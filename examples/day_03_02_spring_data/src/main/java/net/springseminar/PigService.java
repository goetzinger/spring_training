package net.springseminar;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.springframework.stereotype.Component;

@Component
public class PigService {

	public PigService() {
		pigs.add(new Pig("Miss Piggy", 100));
	}

	public List<Pig> pigs = new ArrayList<Pig>();

	public List<Pig> getInhabitants() {
		return Collections.unmodifiableList(pigs);
	}

	public void insert(Pig newPig) {
		pigs.add(newPig);
	}

}