package net.springseminar;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.xml.crypto.dsig.keyinfo.PGPData;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public class PigServiceJPA {

	@PersistenceContext
	private EntityManager manager;
	
	public List<Pig> getAll(){
		TypedQuery<Pig> allPigs = manager.createQuery("SELECT p FROM Pig p",Pig.class);
		//EXECUTE
		List<Pig> resultList = allPigs.getResultList();
		return resultList;
	}
	
	public List<Pig> findByName(String name){
		TypedQuery<Pig> allPigs = manager.createQuery("SELECT p FROM Pig p WHERE p.name LIKE :searchedName",Pig.class);
		//EXECUTE
		List<Pig> resultList = allPigs.setParameter("searchedName","%"+name+"%").getResultList();
		return resultList;
	}
	
	public Pig insert(Pig newPig){
		manager.persist(newPig);
		return newPig;
	}
	
	public boolean remove(String idOfPig){
		Pig pigToRemove = manager.find(Pig.class, idOfPig);
		if(pigToRemove == null){
			return false;
		}
		manager.remove(pigToRemove);
		return true;
	}
	
}
