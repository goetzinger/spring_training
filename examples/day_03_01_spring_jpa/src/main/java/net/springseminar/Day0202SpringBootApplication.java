package net.springseminar;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Day0202SpringBootApplication {

	public static void main(String[] args) {
		SpringApplication.run(Day0202SpringBootApplication.class, args);
	}
}
