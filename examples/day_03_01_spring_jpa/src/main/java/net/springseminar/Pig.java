package net.springseminar;

import java.util.UUID;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Pig {
	
	@Id
	private String id = UUID.randomUUID().toString();

	private String name;
	
	private int weight;

	public Pig() {
		//Used by Jackson
		//GSON
		//JPA
	}
	
	
	public Pig(String name, int weight) {
		super();
		this.name = name;
		this.weight = weight;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getWeight() {
		return weight;
	}

	public void setWeight(int weight) {
		this.weight = weight;
	}
	
	

}
