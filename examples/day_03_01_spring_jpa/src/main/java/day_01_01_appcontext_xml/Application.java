package day_01_01_appcontext_xml;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Application {
	
	public static void main(String[] args) {
		ApplicationContext applicationContext = 
				new AnnotationConfigApplicationContext("day_01_01_appcontext_xml");
		FarmService bean = applicationContext.getBean(FarmService.class);
		bean.feed();
	}

}
