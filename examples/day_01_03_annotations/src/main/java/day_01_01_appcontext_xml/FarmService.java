package day_01_01_appcontext_xml;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import day_01_01_appcontext_xml.service.PigService;

@Component
public class FarmService {

	
	@Autowired
	private PigService pigService;

	public FarmService() {
		System.out.println(this.getClass().getName() + " is created");
	}
	
	
	public void feed() {
		//BUSINESS CODE 
		List<Pig> inhabitants = pigService.getInhabitants();
		inhabitants.stream().forEach(pig -> pig.setWeight(pig.getWeight() + 1));
	}
}
