package day_01_03_annotations;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import day_01_01_appcontext_xml.FarmService;

@RunWith(SpringJUnit4ClassRunner.class)
//Build a spring container from all the information found on that class FarmServiceTest
@ContextConfiguration(classes=FarmServiceTest.class)
@ComponentScan(basePackages="day_01_01_appcontext_xml")
public class FarmServiceTest {
	
	@Autowired
	private FarmService service;
	
	@Test
	public void someTest(){
		service.feed();
	}

}
