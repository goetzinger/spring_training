package net.springseminar.tag_01_02_web;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Tag0102WebApplication {

	public static void main(String[] args) {
		SpringApplication.run(Tag0102WebApplication.class, args);
	}

}
