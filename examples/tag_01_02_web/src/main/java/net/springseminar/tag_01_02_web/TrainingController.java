package net.springseminar.tag_01_02_web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/training") //localhost:8080/training
public class TrainingController {

    private final TrainingService service;

    @Autowired
    public TrainingController(final TrainingService service) {
        this.service = service;
    }

    @GetMapping()//localhost:8080/training
    public List<Training> findAll() {

        return service.findAll();
    }

    @GetMapping("/{id}") //localhost:8080/derGrosseStall
    public ResponseEntity<Training> findByString(@PathVariable("id") final long id) {

        final Optional<Training> byId = service.findById(id);
        if (byId.isPresent()) {
            return ResponseEntity.ok(byId.get());
        }
        return ResponseEntity.notFound().build();
    }


    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping()
    public Training create(@RequestBody final Training toCreate) {
        final Training inserted = service.insert(toCreate);
        return inserted;
    }
}
