package net.springseminar.tag_01_02_web;

import java.util.Calendar;

public class Training {

    private long id;
    private String name;
    private String description;
    private boolean discontinued;
    private Calendar nextRun;
    private String imageUrl;

    public Training() {

    }

    public Training(final String name, final String description) {
        this.name = name;
        this.description = description;
    }

    public long getId() {
        return id;
    }

    public void setId(final long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    public boolean isDiscontinued() {
        return discontinued;
    }

    public void setDiscontinued(final boolean discontinued) {
        this.discontinued = discontinued;
    }

    public Calendar getNextRun() {
        return nextRun;
    }

    public void setNextRun(final Calendar nextRun) {
        this.nextRun = nextRun;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(final String imageUrl) {
        this.imageUrl = imageUrl;
    }
}
