package net.springseminar.tag_01_02_web;

import java.util.HashMap;
import java.util.Map;

public class FarmService {

    private final Map<Long, Farm> dummyFarm = new HashMap<>();

    public FarmService() {
        dummyFarm.put(1L, new Farm());
    }

    public Farm findById(final Long id) {
        return dummyFarm.get(id);
    }
}
