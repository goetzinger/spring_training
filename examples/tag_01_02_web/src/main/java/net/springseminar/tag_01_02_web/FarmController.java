package net.springseminar.tag_01_02_web;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/farm") //localhost:8080/farm
public class FarmController {


    @GetMapping()//localhost:8080/farm
    public List<Farm> findAll() {
        return Arrays.asList(new Farm("Onkel Toms Hütte"));
    }

    @GetMapping("/{name}") //localhost:8080/derGrosseStall
    public ResponseEntity<Farm> findByString(@PathVariable("name") final String name) {
        if (name.isBlank()) {
            return ResponseEntity.badRequest().build();
        }

        final ResponseEntity<Farm> toReturn =
                name.equals("Onkel Toms Hütte")
                        ? ResponseEntity.ok(new Farm("Onkel Toms Hütte")) :
                        ResponseEntity.notFound().build();
        return toReturn;
    }

    @PatchMapping("/{id}")
    public void partialUpdate(@PathVariable final long id, @RequestBody final Map<String, Object> propertiesOfFarm) {
        //suche Farm und setze properties die von client geliefert werden
        //ModelMapper / MapStruct
        if (propertiesOfFarm.containsKey("name")) {

        }

    }


    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping()
    public Farm create(@RequestBody final Farm toCreate, final HttpServletRequest request) {
        //Farm persistieren
        //HttpHeaders.LOCATION : request.getRequestURI() + "/"+toCreate.getId()
        return toCreate;
    }
}
