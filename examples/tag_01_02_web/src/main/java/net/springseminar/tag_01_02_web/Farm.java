package net.springseminar.tag_01_02_web;

public class Farm {

    private String name;

    public Farm() {

    }

    public Farm(final String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }
}
