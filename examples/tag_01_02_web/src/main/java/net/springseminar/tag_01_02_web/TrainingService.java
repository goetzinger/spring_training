package net.springseminar.tag_01_02_web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextStartedEvent;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
public class TrainingService implements ApplicationListener<ContextStartedEvent> {

    @Autowired
    private NamedParameterJdbcTemplate jdbcTemplate;

    @Override
    public void onApplicationEvent(final ContextStartedEvent event) {
        this.insert(new Training("Test", "Testdescription"));
    }


    private final Map<Long, Training> ourStaticDataSource = new HashMap<>();

    public List<Training> findAll() {
        return jdbcTemplate.query("SELECT * FROM Training",
                new BeanPropertyRowMapper<Training>(Training.class));

    }

    public Optional<Training> findById(final long id) {
        return
                Optional.ofNullable(ourStaticDataSource.get(id));
    }

    public Training insert(final Training toInsert) {

        final long key = countNewId();

        ourStaticDataSource.put(key, toInsert);
        toInsert.setId(key);
        final Map<String, Object> params = new HashMap<>();
        params.put("id", toInsert.getId());
        params.put("name", toInsert.getName());
        params.put("description", toInsert.getDescription());
        params.put("nextrun", toInsert.getNextRun());
        params.put("imageurl", toInsert.getImageUrl());
        params.put("discontinued", toInsert.isDiscontinued());

        jdbcTemplate.update("INSERT INTO Training (id, name, description, nextrun, imageurl, discontinued)" +
                        " VALUES (:id, :name, :description, :nextrun, :imageurl, :discontinued)",
                params);
        return toInsert;
    }

    private long countNewId() {
        try {
            return jdbcTemplate.queryForObject("SELECT max(id)+1 FROM Training", new HashMap<>(), Long.class);
        } catch (final EmptyResultDataAccessException e) {
            return 0;
        }
    }


}
