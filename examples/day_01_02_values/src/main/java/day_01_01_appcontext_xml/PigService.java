package day_01_01_appcontext_xml;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class PigService {
	
	private int maxInhabitants;
	
	
	public PigService() {
		System.out.println(this.getClass().getName() + " is created");
	}
	
	public List<Pig> pigs = new ArrayList<Pig>();
	
	public List<Pig> getInhabitants(){
		return Collections.unmodifiableList(pigs);
	}
	
	public void insert(Pig newPig){
		if(pigs.size() < maxInhabitants)
			pigs.add(newPig);
	}
	
	public void setMaxInhabitants(int maxInhabitants) {
		this.maxInhabitants = maxInhabitants;
	}

}