package day_01_01_appcontext_xml;

import java.util.List;

public class FarmService {

	public FarmService() {
		System.out.println(this.getClass().getName() + " is created");
	}
	
	// DI
	private PigService pigService;

	public void feed() {
		//BUSINESS CODE 
		List<Pig> inhabitants = pigService.getInhabitants();
		inhabitants.stream().forEach(pig -> pig.setWeight(pig.getWeight() + 1));
	}
	
	// Spring Calls this method DI
	public void setPigService(PigService pigService) {
		System.out.println("DI in FarmService");
		this.pigService = pigService;
	}
	
	

}
