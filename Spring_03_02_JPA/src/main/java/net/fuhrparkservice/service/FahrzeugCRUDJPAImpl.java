package net.fuhrparkservice.service;

import java.util.List;

import javax.persistence.EntityManager;

import net.fuhrparkservice.model.Fahrzeug;
import net.fuhrparkservice.model.FahrzeugType;

//TODO ANNOTATION
public class FahrzeugCRUDJPAImpl implements IFahrzeugCRUD {

	// TODO ANNOTATION
	private EntityManager manager;

	@Override
	public List<FahrzeugType> getAllFahrzeuge() {
		return manager.createQuery("SELECT f FROM FahrzeugType f",
				FahrzeugType.class).getResultList();
	}

	@Override
	public FahrzeugType getFahrzeugById(String id) {
		return manager.find(FahrzeugType.class, id);
	}

	@Override
	public List<FahrzeugType> getFahrzeugeByFabrikat(String fabrikat) {
		// TODO query
		return null;
	}

	@Override
	public void insert(FahrzeugType fahrzeug2Insert) {
		manager.persist(fahrzeug2Insert);
	}

	@Override
	public void update(FahrzeugType fahrzeug2Update) {
		manager.merge(fahrzeug2Update);
	}

	@Override
	public void remove(String id) {
		Fahrzeug fahrzeug2Remove = manager.find(Fahrzeug.class, id);
		manager.remove(fahrzeug2Remove);
	}

}
