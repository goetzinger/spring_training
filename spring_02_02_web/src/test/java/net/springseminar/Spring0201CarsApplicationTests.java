package net.springseminar;

import org.junit.Assert;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class Spring0201CarsApplicationTests {
	
	@Autowired
	private CarService testCandidate;

	@Before
	public void addSomeCars(){
		testCandidate.insert(new Car("Dieselgate","VW",200,200));
	}
	
	@Test
	public void contextLoads() {
		Assert.assertFalse(testCandidate.findAll().isEmpty());
	}

}
