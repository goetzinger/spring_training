package net.springseminar;

import java.util.List;
import java.util.concurrent.Callable;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/car")
public class CarController {

	@Autowired
	private CarService service;

	@GetMapping //localhost:8080/car?brand=VW
	public List<Car> getAll(
			@RequestParam(name = "brand", required = false) String brand) {
		if (brand == null || brand.isEmpty())
			return service.findAll();
		return findByBrand(brand);
	}

	public List<Car> findByBrand(String brand) {
		return service.findByBrand(brand);
	}
	
	@PostMapping
	public Callable<Car> insert(@RequestBody Car car){
		Callable<Car> taskToDo = new Callable<Car>() {

			@Override
			public Car call() throws Exception {
				System.out.println(Thread.currentThread().getName());
				if(car.getBrand().isEmpty()){
					throw new IllegalArgumentException("");
				}
				service.insert(car);
				return car;
			}
		};
		return taskToDo;
		
	}

}
