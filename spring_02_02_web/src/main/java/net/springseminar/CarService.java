package net.springseminar;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.springframework.stereotype.Component;

@Component
public class CarService {

	private List<Car> carPool = new ArrayList<Car>();
	
	public CarService() {
	carPool.add(new Car("Polo", "VW", 100,180));
	}
	
	public void insert(Car toAdd) {
		//DATABSE Access here
		carPool.add(toAdd);
	}
	
	public boolean remove(Car toRemove){
		return carPool.remove(toRemove);
	}
	
	public List<Car> findAll(){
		return Collections.unmodifiableList(carPool);
	}
	
	public List<Car> findByBrand(String brand){
		List<Car> toReturn = new ArrayList<Car>();
		for (Car car : carPool) {
			if(brand.equals(car.getBrand())){
				toReturn.add(car);
			}
		}
		return toReturn;
	}
	
}
