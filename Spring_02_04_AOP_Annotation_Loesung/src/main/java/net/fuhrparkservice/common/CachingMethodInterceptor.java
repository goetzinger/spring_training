package net.fuhrparkservice.common;

import java.util.HashMap;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;

/**
 * @author goetzingert
 */
@Aspect
public class CachingMethodInterceptor {

	private static Log log = LogFactory.getLog(CachingMethodInterceptor.class);
	private HashMap<Object, Object> cache4ReturnValues = new HashMap<Object, Object>();

	@Pointcut("execution(* net.fuhrparkservice.service.*.get*(..))")
	public void cachingPointcut() {
	}

	@Around("cachingPointcut()")
	public Object around(ProceedingJoinPoint pjp) throws Throwable {
		return handleMethodThatShouldBeCached(pjp);
	}

	private Object handleMethodThatShouldBeCached(ProceedingJoinPoint pjp)
			throws Throwable {
		Object callParameter = getParameter(pjp);
		if (cacheContainsCallParameter(callParameter))
			return getValueFromCache(callParameter);
		else
			return proceedCallAndCache(pjp, callParameter);
	}

	private Object getParameter(ProceedingJoinPoint pjp) {
		return pjp.getArgs()[0];
	}

	private boolean cacheContainsCallParameter(Object callParameter) {
		return this.cache4ReturnValues.containsKey(callParameter);
	}

	private Object proceedCallAndCache(ProceedingJoinPoint pjp,
			Object callParameter) throws Throwable {
		Object returnValue = proceedCall(pjp);
		addValue2Cache(returnValue, callParameter);
		return returnValue;
	}

	private Object proceedCall(ProceedingJoinPoint pjp) throws Throwable {
		return pjp.proceed();
	}

	private void addValue2Cache(Object returnValue, Object callParameter) {
		cache4ReturnValues.put(callParameter, returnValue);
	}

	private Object getValueFromCache(Object callParameter) {
		return cache4ReturnValues.get(callParameter);
	}

}