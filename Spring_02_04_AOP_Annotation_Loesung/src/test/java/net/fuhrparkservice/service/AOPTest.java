package net.fuhrparkservice.service;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Collection;

import javax.annotation.Resource;

import net.fuhrparkservice.model.Fahrzeug;
import net.fuhrparkservice.model.FahrzeugType;
import net.fuhrparkservice.service.FahrzeugServiceDefaultImpl;
import net.fuhrparkservice.service.IFahrzeugCRUD;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "/applicationContext.xml" })
public class AOPTest {
	@Resource(name = "fahrzeugServiceTarget")
	private IFahrzeugCRUD fahrzeugService;

	private IFahrzeugCRUD unCachedFahrzeugService = new FahrzeugServiceDefaultImpl();

	@Test
	public void aopTest() throws Exception {
		Collection<FahrzeugType> firstResult = fahrzeugService
				.getFahrzeugeByFabrikat("VW");
		assertTrue("Caching",
				firstResult == fahrzeugService.getFahrzeugeByFabrikat("VW"));
	}

	@Test
	public void aopTestUnCached() throws Exception {
		Collection<FahrzeugType> firstResult = unCachedFahrzeugService
				.getFahrzeugeByFabrikat("VW");
		assertFalse("Caching",
				firstResult == unCachedFahrzeugService
						.getFahrzeugeByFabrikat("VW"));
	}

}
