webapp-javaee7 archetype

0. pom.xml

<failOnMissingWebXml>false</failOnMissingWebXml>
                </configuration>


1. Class extends AbstractAnnotationConfigDispatcherServletInitializer

 @Override
    protected Class<?>[] getRootConfigClasses() {
        return new Class<?>[0];
    }

    @Override
    protected Class<?>[] getServletConfigClasses() {
        return new Class<?>[]{WebMVCConfiguration.class};
    }

    @Override
    protected String[] getServletMappings() {
        return new String[]{"/"};
    }

2. MVC Config
@EnableWebMvc
@Configuration
@Import(RestDataMvcConfiguration.class)
@ComponentScan(basePackages = {"net.fuhrparkservice.web"})
public class WebMVCConfiguration extends WebMvcConfigurerAdapter
{

3. RestConfig

@Configuration
public class RestDataMvcConfiguration extends RepositoryRestMvcConfiguration {
protected void configureJacksonObjectMapper(ObjectMapper objectMapper) {
