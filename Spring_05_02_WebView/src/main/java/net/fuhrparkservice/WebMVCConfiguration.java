/**
 * Copyright (c) Deere & Company as an unpublished work
 * THIS SOFTWARE AND/OR MATERIAL IS THE PROPERTY OF
 * DEERE & COMPANY.  ALL USE, DISCLOSURE, AND/OR
 * REPRODUCTION NOT SPECIFICALLY AUTHORIZED BY
 * DEERE & COMPANY IS PROHIBITED.
 */
package net.fuhrparkservice;

import org.springframework.context.MessageSource;
import org.springframework.context.annotation.*;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.rest.webmvc.config.RepositoryRestMvcConfiguration;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewResolverRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.thymeleaf.spring4.SpringTemplateEngine;
import org.thymeleaf.spring4.templateresolver.SpringResourceTemplateResolver;
import org.thymeleaf.spring4.view.ThymeleafViewResolver;
import org.thymeleaf.templateresolver.ITemplateResolver;

import java.util.List;

/**
 * Created by goetzingert on 01.12.15.
 */
@EnableWebMvc
@Configuration
@Import(RestDataMvcConfiguration.class)
@EnableJpaRepositories(basePackages = {"net.fuhrparkservice.service"})
@ImportResource("classpath:applicationContext.xml")
@ComponentScan(basePackages = {"net.fuhrparkservice.web"})
public class WebMVCConfiguration extends WebMvcConfigurerAdapter
{




    @Override
    public void configureViewResolvers(ViewResolverRegistry registry) {
        registry.viewResolver(viewResolver());
    }

    @Bean
    public ThymeleafViewResolver viewResolver() {
        final ThymeleafViewResolver thymeleafViewResolver = new ThymeleafViewResolver();
        thymeleafViewResolver.setTemplateEngine(templateEngine());
        return thymeleafViewResolver;
    }

    @Bean
    public MessageSource messageSource(){
        final ResourceBundleMessageSource resourceBundleMessageSource = new ResourceBundleMessageSource();
        resourceBundleMessageSource.setBasename("messages");
        return resourceBundleMessageSource;
    }

    @Bean
    public SpringTemplateEngine templateEngine() {
        final SpringTemplateEngine springTemplateEngine = new SpringTemplateEngine();
        springTemplateEngine.setTemplateResolver(templateResolver());

        return springTemplateEngine;
    }



    @Bean
    public ITemplateResolver templateResolver() {
        final SpringResourceTemplateResolver servletContextTemplateResolver = new SpringResourceTemplateResolver();
        servletContextTemplateResolver.setPrefix("classpath:templates/");
        servletContextTemplateResolver.setSuffix(".xhtml");
        servletContextTemplateResolver.setTemplateMode("HTML5");
        return servletContextTemplateResolver;
    }


}
