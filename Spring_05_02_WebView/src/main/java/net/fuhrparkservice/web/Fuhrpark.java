package net.fuhrparkservice.web;

import net.fuhrparkservice.model.FahrzeugType;
import net.fuhrparkservice.service.IFahrzeugTypeCRUD;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller()
@RequestMapping("/fahrzeuge")
public class Fuhrpark {
	
	@Autowired
	private IFahrzeugTypeCRUD fahrzeugCRUD;
	

	@ModelAttribute("allCartypes")
	public Iterable<FahrzeugType> getAllFahrzeuge()
	{
		return fahrzeugCRUD.findAll();
	}
	
	@RequestMapping({"/","/overview"})
	public String showOverview() {
	    return "overview";
	}
	
	@RequestMapping(value = "/add",method=RequestMethod.GET)
	public String startAdd(Model model){
		model.addAttribute("cartype", new FahrzeugType());
		return "addCar";
	}
	
	@RequestMapping(value="/addCarSubmit", method=RequestMethod.POST)
	public String add(final FahrzeugType cartype, BindingResult result, ModelMap model){
		if(result.hasErrors())
			return "addCar";
		model.clear();
		fahrzeugCRUD.save(cartype);
		return "redirect:overview";
	}
}
