/**
 * Copyright (c) Deere & Company as an unpublished work
 * THIS SOFTWARE AND/OR MATERIAL IS THE PROPERTY OF
 * DEERE & COMPANY.  ALL USE, DISCLOSURE, AND/OR
 * REPRODUCTION NOT SPECIFICALLY AUTHORIZED BY
 * DEERE & COMPANY IS PROHIBITED.
 */
package net.fuhrparkservice.web;

import net.fuhrparkservice.model.Filiale;
import net.fuhrparkservice.service.IFilialeCRUD;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.hateoas.LinkBuilderFactory;
import org.springframework.hateoas.MethodLinkBuilderFactory;
import org.springframework.hateoas.ResourceSupport;
import org.springframework.hateoas.Resources;
import org.springframework.hateoas.mvc.ControllerLinkBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by goetzingert on 02.12.15.
 */
@RepositoryRestController
public class FilialeController {


    @Autowired
    private  IFilialeCRUD repository;

    @RequestMapping(method = RequestMethod.GET, value = "/filiales/search/list")
    public @ResponseBody ResponseEntity<?> getAll(){
        final Iterable<Filiale> all = repository.findAll();
        Resources<Filiale> resources = new Resources<Filiale>(all);

        resources.add(ControllerLinkBuilder.linkTo(ControllerLinkBuilder.methodOn(FilialeController.class).getAll()).withSelfRel());

        // add other links as needed

        return ResponseEntity.ok(resources);
    }
}
