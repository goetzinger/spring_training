/**
 * Copyright (c) Deere & Company as an unpublished work
 * THIS SOFTWARE AND/OR MATERIAL IS THE PROPERTY OF
 * DEERE & COMPANY.  ALL USE, DISCLOSURE, AND/OR
 * REPRODUCTION NOT SPECIFICALLY AUTHORIZED BY
 * DEERE & COMPANY IS PROHIBITED.
 */
package net.fuhrparkservice.model;

import org.springframework.data.rest.core.config.Projection;

/**
 * Created by goetzingert on 02.12.15.
 */
@Projection(types = FahrzeugType.class)
interface ModelAndFabrikat {

    long getPs();

    long getMaxKmH();
}
