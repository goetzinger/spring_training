/**
 * 
 */
package net.fuhrparkservice.model;

import javax.persistence.Embeddable;

/**
 * @author goetzingert
 * 
 */
@Embeddable
public class Model {

	private String fabrikat;
	private String modell;

	public Model() {
	}

	public Model(String fabrikat, String modell) {
		super();
		this.fabrikat = fabrikat;
		this.modell = modell;
	}

	public String getFabrikat() {
		return fabrikat;
	}

	public void setFabrikat(String fabrikat) {
		this.fabrikat = fabrikat;
	}

	public String getModell() {
		return modell;
	}

	public void setModell(String modell) {
		this.modell = modell;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((fabrikat == null) ? 0 : fabrikat.hashCode());
		result = prime * result + ((modell == null) ? 0 : modell.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Model other = (Model) obj;
		if (fabrikat == null) {
			if (other.fabrikat != null)
				return false;
		} else if (!fabrikat.equals(other.fabrikat))
			return false;
		if (modell == null) {
			if (other.modell != null)
				return false;
		} else if (!modell.equals(other.modell))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Model [fabrikat=" + fabrikat + ", modell=" + modell + "]";
	}


}
