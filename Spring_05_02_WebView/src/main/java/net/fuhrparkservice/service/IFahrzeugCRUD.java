package net.fuhrparkservice.service;

import net.fuhrparkservice.model.Fahrzeug;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 * Created by goetzingert on 02.12.15.
 */
public interface IFahrzeugCRUD extends PagingAndSortingRepository<Fahrzeug, String> {
}
