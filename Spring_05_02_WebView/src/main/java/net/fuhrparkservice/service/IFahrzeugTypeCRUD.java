package net.fuhrparkservice.service;

import net.fuhrparkservice.model.FahrzeugType;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RestResource;

import java.util.List;


public interface IFahrzeugTypeCRUD extends CrudRepository<FahrzeugType, String> {


    @RestResource(path = "byFabrikat", rel = "byFabrikat")
    List<FahrzeugType> findByModelFabrikat(@Param("fabrikat") String fabrikat);


}
