/**
 * Copyright (c) Deere & Company as an unpublished work
 * THIS SOFTWARE AND/OR MATERIAL IS THE PROPERTY OF
 * DEERE & COMPANY.  ALL USE, DISCLOSURE, AND/OR
 * REPRODUCTION NOT SPECIFICALLY AUTHORIZED BY
 * DEERE & COMPANY IS PROHIBITED.
 */
package net.fuhrparkservice.service;

import net.fuhrparkservice.model.Filiale;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 * Created by goetzingert on 02.12.15.
 */

public interface IFilialeCRUD extends PagingAndSortingRepository<Filiale,String>{
}
